package com.example.wifimonitor4;

import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

public class MonitorVideo extends Activity {
	
	private WebView webViewTempe;
	private String addressString;
	
	

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
//		// 显示视频监控界面
		setContentView(R.layout.monitor_video);		
		
		webViewTempe = (WebView) findViewById(R.id.WebViewTempe);
		
		// 全屏运行
//		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
//		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		// 获取远端视频监控设备地址
		Intent intent = getIntent();
		addressString = intent.getStringExtra("address");
		Log.i("address", addressString);


		// 设置SurfaceView地址参数
		MontiorSurfaceView r = (MontiorSurfaceView) findViewById(R.id.SurfaceViewVideo);
		if (r == null) {
			Log.e("r", "r == null");
		}
		r.setMontiorAddress(addressString);
		Log.i("address", "setMontiorAddress complete");
		
		//加载tempe和GPS两个webview
				webViewTempe.loadUrl(addressString+"/temperature.txt");
				//覆盖WebView默认通过浏览器打开网页的行为
				webViewTempe.setWebViewClient(new WebViewClient(){
					@Override
					public boolean shouldOverrideUrlLoading(WebView view, String url) {
						// TODO Auto-generated method stub
						view.loadUrl(url);
						//返回true则在WebView中打开，返回false则调用浏览器打开
						return true;
					}
				});
				
				Timer time = new Timer();  
		        Integer a = new Integer(0) ;  
		        RefreshWebView t = new RefreshWebView();  
		        //一秒后执行，每1秒执行一次  
		        time.schedule(t, 1000,1000);
		
	}
	
	class RefreshWebView extends TimerTask
	{

		@Override
		public void run() {
			// TODO Auto-generated method stub
			webViewTempe.loadUrl(addressString+"/temperature.txt");
		}
		
	}

	private long exitTime = 0;

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN) {

			if ((System.currentTimeMillis() - exitTime) > 2000) {
				Toast.makeText(getApplicationContext(), "再按一次退出程序", Toast.LENGTH_SHORT).show();
				exitTime = System.currentTimeMillis();
			} else {
				finish();
				System.exit(0);
			}

			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
}
