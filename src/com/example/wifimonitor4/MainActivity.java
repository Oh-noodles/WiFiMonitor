package com.example.wifimonitor4;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends Activity {

	//声明控件
	private EditText editTextAddress;
	private Button buttonClose;
	private Button buttonConnect;
	private Button buttonMap;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		//实例化控件
		editTextAddress = (EditText) findViewById(R.id.EditTextAddress);
		buttonClose = (Button) findViewById(R.id.buttonClose);
		buttonConnect = (Button) findViewById(R.id.buttonConnect);
		buttonMap = (Button) findViewById(R.id.buttonMap);
		
		
		//为connect按钮注册监听事件
		buttonConnect.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				//获取EditText内的内容存入address字符串
				String address = editTextAddress.getText().toString();
				
				//新建Intent对象，并添加键值对
				Intent intent = new Intent();
				intent.putExtra("address", address);
				//设置要启动的Activity
				intent.setClass(MainActivity.this, MonitorVideo.class);
				//启动Activity
				MainActivity.this.startActivity(intent);
			}
		});
		
		//为buttonMap按钮注册监听事件
		buttonMap.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//新建intent对象
				Intent intent = new Intent();
				//设置要启动的Activity
				intent.setClass(MainActivity.this, MapActivity.class);
				//启动Activity
				MainActivity.this.startActivity(intent);
			}
		});
		
		//为close按键注册监听事件
		buttonClose.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
				System.exit(0);
			}
		});
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
